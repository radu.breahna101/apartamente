docker=docker-apartments #docker folder
workspace-exec=cd $(docker) && docker-compose exec php

install:
	composer install --ignore-platform-reqs
	composer dump-autoload
update:
	composer update --ignore-platform-reqs
clean:
	composer install --ignore-platform-reqs
	composer dump-autoload
	$(workspace-exec) php artisan cache:clear
	$(workspace-exec) php artisan config:clear
dev:
	cd $(docker) && docker-compose up -d php mysql nginx
	#cd $(docker) && docker-compose exec workspace php artisan io:server start
elastic:
	cd $(docker) && docker-compose up -d elasticsearch logstash kibana
patch:
	$(workspace-exec) php artisan deploy:patch
migrate:
	$(workspace-exec) php artisan migrate
rollback:
	$(workspace-exec) php artisan migrate:rollback --step=1
seed-entities:
	$(workspace-exec) php artisan db:seed --class=RealDataSeeder
seed-profiles:
	$(workspace-exec) php artisan db:seed --class=ProfilesTableSeeder
hash:
	$(workspace-exec) php artisan hash:imsi
dev2:
	cd $(docker) && docker-compose up -d mysql nginx phpmyadmin redis
pm2-logs:
	$(workspace-exec) pm2 logs
down:
	cd $(docker) && docker-compose down
ps:
	cd $(docker) && docker-compose ps
rebuild:
	$(workspace-exec) php artisan rebuild:app
docs:
	$(workspace-exec) php artisan docs:generate
dump:
	$(workspace-exec) composer dump-autoload
