<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    protected $fillable = [
        'title',
        'description',
        'address',
        'rooms',
        'price',
        'user_id',
    ];
}
