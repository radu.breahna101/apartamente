<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ROLE_ADMIN = 'admin';

    const ROLE_USER  = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function apartments()
    {
        return $this->hasMany(Apartment::class);
    }

    public function cartApartments()
    {
        return $this->belongsToMany(Apartment::class)->withPivot('bought')->whereBought(0)->orderByDesc('updated_at');
    }

    public function orders()
    {
        return $this->belongsToMany(Apartment::class)->withPivot('bought')->whereBought(1)->orderByDesc('updated_at');
    }
}
