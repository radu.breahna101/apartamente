<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RebuildApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rebuild:app';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuilds Application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('migrate:fresh', ['--seed' => 'default']);
        //$this->call('passport:install', ['--force' => 'default']);
        $this->call('cache:clear');
        $this->call('config:clear');
        // $this->call('clear:log');
        // $this->call('storage:clear');
        $this->alert('Running chmod...');
        exec('chmod 777 -R storage/ bootstrap/cache', $output);
        $this->comment(implode(PHP_EOL, $output));
        //$this->alert('Cleaning storage');
        //$this->call('storage:clear');
        // $this->alert('Installing telescope');
        //$this->call('telescope:install');
        $this->call('storage:link');
        $this->alert('Finished!');

        return;
    }
}
