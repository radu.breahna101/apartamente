<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Http\Requests\SortAndFilterRequest;
use App\Http\Requests\UpdateApartmentRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApartmentController extends Controller
{

    public function buy(Request $request)
    {
        $user = Auth::user();
        $user->cartApartments()->update(['bought' => 1]);

        return $user->orders;
    }

    public function orders()
    {
        $user = Auth::user();

        return $user->orders;
    }

    public function cart()
    {
        $user = Auth::user();

        return $user->cartApartments;
    }

    public function addToCart(Apartment $apartment)
    {
        $user = Auth::user();
        $user->cartApartments()->syncWithoutDetaching($apartment->id);

        return $user->cartApartments;
        //$user->roles()->attach($roleId, ['expires' => $expires]);
    }

    public function removeFromCart(Apartment $apartment)
    {
        $user = Auth::user();
        $user->cartApartments()->detach($apartment->id);

        return $user->cartApartments;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bought = DB::table('apartment_user')->where('bought', '=', 1)->get()->pluck('apartment_id');

        return Apartment::whereNotIn('id', $bought)->orderByDesc('updated_at')->paginate(5);
    }

    public function sortAndFilter(SortAndFilterRequest $request)
    {

        $bought = DB::table('apartment_user')->where('bought', '=', 1)->get()->pluck('apartment_id');

        $query = Apartment::whereNotIn('id', $bought);

        if ($request->has('description') && ! empty($request->description)) {
            $query = $query->where('description', 'like', '%'.$request->description.'%');
        }

        if ($request->has('address') && ! empty($request->address)) {
            $query = $query->where('address', 'like', '%'.$request->address.'%');
        }

        if ($request->has('title') && ! empty($request->title)) {
            $query = $query->where('title', 'like', '%'.$request->title.'%');
        }

        if ($request->has('priceMin') && $request->has('priceMax')) {

            if ($request->priceMin !== '' && $request->priceMin !== null) {
                $query = $query->where('price', '>', $request->priceMin);
            }
            if (! empty($request->priceMax)) {
                $query = $query->where('price', '<', $request->priceMax);
            }
        }

        if ($request->has('rooms') && $request->rooms !== '' && $request->rooms !== null) {
            $query = $query->where('rooms', $request->rooms);
        }

        if ($request->has('orderBy') && ! empty($request->orderBy)) {

            if ($request->has('direction') && $request->direction == 'desc') {
                $query = $query->orderByDesc($request->orderBy);
            } else {
                $query = $query->orderBy($request->orderBy);
            }

            // dd($query->toSql());

            return $query->paginate(5);
        }

        return $query->orderByDesc('updated_at')->paginate(5);
    }

    public function show(Apartment $apartment)
    {
        return $apartment;
    }

    /**
     * @param \App\Http\Requests\UpdateApartmentRequest $request
     * @return mixed
     */
    public function store(UpdateApartmentRequest $request)
    {
        return Apartment::create(array_merge($request->all(), ['user_id' => Auth::id()]));
    }

    /**
     * @param \App\Http\Requests\UpdateApartmentRequest $request
     * @param \App\Apartment $apartment
     * @return bool
     */
    public function update(UpdateApartmentRequest $request, Apartment $apartment)
    {
        return $apartment->update($request->all());
    }

    /**
     * @param \App\Apartment $apartment
     * @return bool|null
     * @throws \Exception
     */
    public function destroy(Apartment $apartment)
    {
        return $apartment->delete();
    }
}
