<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateApartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'string|required|min:10',
            'description' => 'string|required|min:10',
            'address'     => 'string|required|min:10',
            'rooms'       => 'required|integer|min:1|max:100',
            'price'       => 'required|integer|min:0',
        ];
    }
}
