import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './router';
import App from "./components/App";
import store from './store/index';
import Axios from 'axios'
import Vuex from 'vuex'
//import { sync } from 'vuex-router-sync'


//import bootstrap from 'bootstrap';
// Before you create app
//Vue.config.devtools = process.env.NODE_ENV === 'development'


Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
Axios.defaults.baseURL = 'http://apartments.test/'
Axios.defaults.withCredentials = true;
Vue.prototype.$http = Axios;
Vuex.Store.prototype.$http = Axios;

Vue.use(VueRouter);


//const unsync = sync(store, router)
const app = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');

//unsync()
//window.__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue = app.constructor

// /**
//  * First we will load all of this project's JavaScript dependencies which
//  * includes Vue and other libraries. It is a great starting point when
//  * building robust, powerful web applications using Vue and Laravel.
//  */
//
 //require('./bootstrap');
//
//  window.Vue = require('vue');
//
// /**
//  * The following block of code may be used to automatically register your
//  * Vue components. It will recursively scan this directory for the Vue
//  * components and automatically register them with their "basename".
//  *
//  * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
//  */
//
// // const files = require.context('./', true, /\.vue$/i)
// // files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
//
 //Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// const app = new Vue({
//     el: '#app',
// });
