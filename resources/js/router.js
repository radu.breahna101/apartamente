import Homepage from "./components/Homepage";
import AdminPage from "./components/AdminPage";
import Login from "./components/Login";
import NotFound from './components/NotFound';
import Unauthorized from "./components/Unauthorized";
import Error from "./components/Error";
import Cart from "./components/Cart";
import store from './store/index';
//https://router.vuejs.org/guide/advanced/navigation-guards.html#in-component-guards

import VueRouter from 'vue-router';
import EditingListing from "./components/EditingListing";
import AddListing from "./components/AddListing";
import Register from "./components/Register";

//https://garywoodfine.com/using-vuex-and-vue-router-in-a-vue-project/

const routes = {
    mode: 'history',
    linkActiveClass: "active",
    routes: [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/unauthorized',
            component: Unauthorized
        },
        {
            path: '/error',
            component: Error
        },
        {
            path: '/',
            component: Homepage
        },
        {
            path: '/admin',
            component: AdminPage,
            meta: {
                admin: true
            }
        },
        {
            path: '/login',
            component: Login,

        },
        {
            path: '/cart',
            component: Cart,

        },
        {
            path: '/editing-listing/:id',
            component: EditingListing
        },
        {
            path: '/add-listing',
            component: AddListing
        },
        {
            path: '/register',
            component: Register
        }

    ]
}

const router = new VueRouter(routes)

// router.afterEach((to, from) => {
//     // ...
// })

router.onError((error) => {
    if (error.response) {
        // console.log(error.response.data);
        // console.log(error.response.status);
        // console.log(error.response.headers);
        store.dispatch('setAuthErrorMessage', error.response.data.message)
    }

})


router.beforeEach((to, from, next) => {
    if ((to.path === '/login') || (to.path === '/register')) {
        next()
        return
    }
    store.dispatch('getAuthUser').then((response) => {
        store.dispatch('setAuthUser', response.data).then(() => next())
    }).catch(error => {
        next('/login')
    })


})

// router.beforeEach((to, from, next) => {
//     if(store.getters.authError !== ''){
//         next('/error')
//     }
// })


router.beforeEach((to, from, next) => {
    if (to.path === '/login' && store.getters.isLoggedIn) {
        // if (store.getters.userRole === 'user') {
        //     next('/')
        // } else if (store.getters.userRole === 'admin') {
        //     next('/admin')
        // }
        next('/')
    } else {
        next()
    }
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.admin)) {
        if (store.getters.userRole === 'admin') {
            next()
            return
        }
        next('/unauthorized')
    } else {
        next()
    }
})


export default router
