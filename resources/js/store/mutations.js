export const mutations = {


    setForm(state, form){
        state.form = form
    },
    setCart(state, listings){
        state.cart.listings = listings
    },

    setOrders(state, listings){
        state.orders.listings = listings
    },

    setListings(state, listings){
        state.listings = listings
    },

    setCurrentPage(state, currentPage){
        state.currentPage = currentPage
    },

    setLastPage(state, lastPage){
        state.lastPage = lastPage
    },

    setAuthErrorMessage(state, message) {
        state.auth.errorMessage = message
    },

    setAuthUser(state, user) {
        state.auth.user = user
        state.auth.loggedIn = true
        state.auth.role = user.role
        state.auth.errorMessage = ''
    },

    logout(state) {
        state.auth.user = ''
        state.auth.loggedIn = false
        state.auth.role = ''
    },

    addTodo(state, todo) {
        state.todos.push(todo)
    },

    removeTodo(state, todo) {
        state.todos.splice(state.todos.indexOf(todo), 1)
    },

    editTodo(state, {todo, text = todo.text, done = todo.done}) {
        const index = state.todos.indexOf(todo)

        state.todos.splice(index, 1, {
            ...todo,
            text,
            done
        })
    }
}
