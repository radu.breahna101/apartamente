import router from "../router"


export default {

//https://scotch.io/tutorials/handling-authentication-in-vue-using-vuex
    async getCsrf() {
        return await this.$http.get('/sanctum/csrf-cookie')
    },

    async login({dispatch}, form) {
        await dispatch('getCsrf')
        return await form.post('/api/login')
    },

    async register({dispatch}, form) {
        return await form.post('/api/register')
    },

    async updateListing({dispatch}, form) {
        return await form.post('/api/update-listings/' + form.id)
    },

    async addListing({dispatch}, form) {
        return await form.post('/api/add-listing')
    },

    async deleteListing({dispatch}, id) {
        return await this.$http.delete('/api/delete-listing/' + id)
    },


    async logout({commit}) {
        await this.$http.get('/api/logout')
        commit('logout');
        await router.push('/login')
    },

    setAuthUser({commit}, user) {
        commit('setAuthUser', user)
    },

    setForm({commit}, form) {
        commit('setForm', form)
    },

    setAuthErrorMessage({commit}, message) {
        commit('setAuthErrorMessage', message)
    },

    async getAuthUser({dispatch}) {
        return await this.$http.get('/api/user')
    },

    async getUsers({dispatch}) {
        return await this.$http.get('/api/users')
    },

    async getListings({dispatch}, form) {
        return await form.post('/api/sort-and-filter?page=' + form.page)
    },

    async getListing({dispatch}, id) {
        return await this.$http.get('/api/listings/' + id)
    },

    async addToCart({dispatch}, apartmentId) {
        return await this.$http.get('/api/add-to-cart/' + apartmentId)
    },
    async removeFromCart({dispatch}, apartmentId) {
        return await this.$http.get('/api/remove-from-cart/' + apartmentId)
    },

    async getCart({dispatch}) {
        return await this.$http.get('/api/cart/')
    },


    async getOrders({dispatch}) {
        return await this.$http.get('/api/orders/')
    },

    async buy({dispatch}, listingIds) {
        return await this.$http.post('/api/buy/', {'apartment_ids': listingIds})
    },


    setListings({commit}, response) {
        commit('setListings', response)
    },

    setCart({commit}, data) {
        commit('setCart', data)
    },

    setOrders({commit}, data) {
        commit('setOrders', data)
    },

    setCurrentPage({commit}, currentPage) {
        commit('setCurrentPage', currentPage)
    },

    setLastPage({commit}, lastPage) {
        commit('setLastPage', lastPage)
    },

    addTodo({commit}, text) {
        commit('addTodo', {
            text,
            done: false
        })
    },

    removeTodo({commit}, todo) {
        commit('removeTodo', todo)
    },

    toggleTodo({commit}, todo) {
        commit('editTodo', {todo, done: !todo.done})
    },

    editTodo({commit}, {todo, value}) {
        commit('editTodo', {todo, text: value})
    },

    toggleAll({state, commit}, done) {
        state.todos.forEach((todo) => {
            commit('editTodo', {todo, done})
        })
    },

    clearCompleted({state, commit}) {
        state.todos.filter(todo => todo.done)
            .forEach(todo => {
                commit('removeTodo', todo)
            })
    }
}
