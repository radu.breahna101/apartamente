export default {
    userRole: state => state.auth.user.role,
    userName: state => state.auth.user.username,
    user: state => state.auth.user,
    isLoggedIn: state => state.auth.loggedIn,
    authError: state => state.auth.errorMessage,
    currentPage: state => state.currentPage,
    lastPage: state => state.lastPage,
    cartCount: state => state.cart.listings.length,
    ordersCount: state => state.orders.listings.length,
    isInCart: (state) => (id) => {
        return (state.cart.listings.find(listing => listing.id === id) !== undefined);
    },
    getListing: (state) => (id) => {
        return state.listings.find(listing => listing.id === id);
    },
}
