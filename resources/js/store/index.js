import Vue from 'vue'
import Vuex from 'vuex'
import {mutations,} from './mutations'
import actions from './actions'
import getters from "./getters";
import Form from 'form-backend-validation';


Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        form: new Form(),
        auth: {
            login: '',
            password: '',
            role: '',
            user: '',
            loggedIn: false,
            errorMessage: '',
        },
        listings: [],
        currentPage: 1,
        lastPage: 1,
        cart: {
            listings: []
        },
        orders: {
            listings: []
        }
    },
    mutations,
    actions,
    getters,
})
