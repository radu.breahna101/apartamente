<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Apartamente</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{mix('/css/app.css')}}">
</head>

<body class="font-sans">
<div id="app">
    <router-view>
    </router-view>
</div>

<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
