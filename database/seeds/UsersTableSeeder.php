<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Admin',
            'username' => 'admin',
            'role'     => User::ROLE_ADMIN,
            'password' => \Illuminate\Support\Facades\Hash::make('qwerty1'),
        ]);

        User::create([
            'name'     => 'User',
            'username' => 'user',
            'role'     => User::ROLE_USER,
            'password' => \Illuminate\Support\Facades\Hash::make('qwerty'),
        ]);
    }
}
