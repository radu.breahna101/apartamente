<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Faker\Provider\Lorem;

$factory->define(\App\Apartment::class, function (Faker $faker) {
    $faker->addProvider(new Lorem($faker));

    return [
        'user_id'     => $faker->numberBetween(1, 2),
        'title'       => $faker->sentence,
        'description' => $faker->realText(250),
        'address'     => $faker->address,
        'rooms'       => $faker->numberBetween(1, 5),
        'price'       => $faker->numberBetween(10000, 130000),
    ];
});
