<?php

use App\Http\Controllers\ApartmentController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/login', [AuthController::class, 'authenticate']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('/sort-and-filter', [ApartmentController::class, 'sortAndFilter']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('/users', [AuthController::class, 'index']);
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/listings', [ApartmentController::class, 'index']);

    Route::post('/update-listings/{apartment}', [ApartmentController::class, 'update']);
    Route::post('/add-listing', [ApartmentController::class, 'store']);
    Route::delete('/delete-listing/{apartment}', [ApartmentController::class, 'destroy']);
    Route::get('/listings/{apartment}', [ApartmentController::class, 'show']);
    Route::post('/buy', [ApartmentController::class, 'buy']);
    Route::get('/cart', [ApartmentController::class, 'cart']);
    Route::get('/orders', [ApartmentController::class, 'orders']);
    Route::get('/add-to-cart/{apartment}', [ApartmentController::class, 'addToCart']);
    Route::get('/remove-from-cart/{apartment}', [ApartmentController::class, 'removeFromCart']);
});

